<?php

namespace TBot;

use DateTime;
use Exception;

class Operation extends Item
{
    public $operationType;
    /**
     * @var DateTime
     */
    public $date;
    public $isMarginCall;
    public $instrumentType;
    public $figi;
    public $quantity;
    public $price;
    public $payment;
    public $realPayment;
    public $currency;
    public $commission;
    public $trades;
    public $status;
    public $id;

    public function fromArray(array $data)
    {
        parent::fromArray($data);

        $this
            ->setDate($data['date'])
            ->setRealPayment();

        return $this;
    }

    public function toArray() : array
    {
        $result = parent::toArray();

        $result['date'] = $this->date->getTimestamp();

        return $result;
    }

    public function setDate($value)
    {
        if (is_numeric($value)) {
            $this->date = new DateTime('@' . $value);
        } elseif (is_string($value)) {
            $this->date = new DateTime($value);
        } elseif ($value instanceof DateTime) {
            $this->date = $value;
        } elseif (is_array($value)) {
            $this->date = new DateTime();
            $this->date->__set_state($value);
        } else {
            throw new Exception('Unknown type of value to set date');
        }

        return $this;
    }

    public function setRealPayment()
    {
        $this->realPayment = $this->payment + ($this->commission['value'] ?? 0);

        return $this;
    }

    public function getUniqueKey()
    {
        $arr = $this->toArray();
        unset($arr['trades']);
        return $this->getHash($arr);
    }

}
