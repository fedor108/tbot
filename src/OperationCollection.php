<?php

namespace TBot;

use Exception;
use DateTime;
use Dotenv\Dotenv;

class OperationCollection extends Collection
{
    protected $storage;

    public $loadedFromStorage = [];
    public $loadedFromApi = [];

    protected function init($params = [])
    {
        $this->itemClass = Operation::class;

        $this->storage = new FileStorage('operations');

        return $this;
    }

    public function loadFromStorage()
    {
        $this->loadedFromStorage = $this->storage->read();
        $this->attachArray($this->loadedFromStorage);

        return $this;
    }

    public function load($params = [])
    {
        $this->loadFromStorage();

        $from = empty($params['from']) ? $this->getFrom() : $params['from'];
        $to = empty($params['to']) ? new DateTime('now') : $params['to'];
        $figi = $params['figi'] ?? null;

        try {
            $this->loadedFromApi = $this->client->getOperations($from, $to, $figi);
        } catch (Exception $exception) {
            throw new Exception('Can\'t load operations: ' . $exception->getMessage());
        }

        if (!empty($this->loadedFromApi)) {
            $this->attachArray($this->loadedFromApi);
        }

        return $this;
    }

    protected function getFrom()
    {
        if (empty($this->data)) {
            $dotenv = Dotenv::create(__DIR__ . '/../');
            $dotenv->load();
            return new DateTime(getenv('FIRST_OPERATION_DATE'));
        } else {
            return (clone end($this->data)->date)->add(new \DateInterval('PT1S'));
        }
    }

    public function attachArray(array $data)
    {
        parent::attachArray($data);

        $this->sortBy('date');

        return $this;
    }

    public function save()
    {
        return $this->storage->write($this->toArray());
    }

    public function getApiCount()
    {
        return count($this->loadedFromApi);
    }

    public function getRealPaymentSum()
    {
        return array_reduce($this->data, function ($carry, Operation $operation) {
            $carry += $operation->realPayment;
            return $carry;
        }, 0);
    }
}
