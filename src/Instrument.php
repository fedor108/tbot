<?php

namespace TBot;

class Instrument extends Item
{
    public $figi;
    public $ticker;
    public $isin;
    public $minPriceIncrement;
    public $lot;
    public $currency;
    public $name;
    public $type;

    public $title;

    /**
     * @var TClient $client
     */
    protected $client;

    protected function init() {
        $this->client = new TClient();

        $this->setTitle();

        return $this;
    }

    protected function setTitle()
    {
        $this->title = empty($this->ticker) ? $this->currency : "{$this->name} {$this->ticker} {$this->figi}";

        return $this;
    }

    public function getCurrentPrice()
    {
        $response = $this->client->getOrderbook($this->figi, 1);

        return $response['lastPrice'] ?? null;
    }

    public function toArray() : array
    {
        $arr = parent::toArray();

        unset($arr['client']);

        return $arr;
    }
}
