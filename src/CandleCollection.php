<?php

namespace TBot;

use Exception;
use DateTime;
use DateInterval;
use Dotenv\Dotenv;

class CandleCollection extends Collection
{
    const INTERVALS = [
        '1min', '2min', '3min', '5min', '10min', '15min', '30min', 'hour', 'day', 'week', 'month',
    ];

    protected $figi;
    protected $interval;

    /**
     * @var StorageInterface $storage
     */
    protected $storage;

    protected function init($params = [])
    {
        if (empty($params['figi'])) {
            throw new Exception('Empty figi');
        }
        $this->figi = $params['figi'];

        if (empty($params['interval'])) {
            throw new Exception('Empty interval');
        }
        $this->interval = $params['interval'];

        $this->itemClass = Candle::class;

        $this->storage = new FileStorage("candles-{$this->figi}-{$this->interval}");

        return $this;
    }

    public function load($params = [])
    {
        $this->loadFromStorage();

        $from = empty($params['from']) ? $this->getFrom() : $params['from'];
        $to = empty($params['to']) ? new DateTime('now') : $params['to'];

        $nextMonth = clone $from;
        $nextMonth->add(new DateInterval('P1M'));

        while ($nextMonth < $to && $from->format('Y-m-d') < $to->format('Y-m-d')) {
            $this->attachArray($this->client->getCandles($this->figi, $from, $nextMonth, $this->interval));
            $nextMonth->add(new DateInterval('P1M'));
            $from->add(new DateInterval('P1M'));
            usleep(500000);
        }

        if ($from->format('Y-m-d') < $to->format('Y-m-d')) {
            $this->attachArray($this->client->getCandles($this->figi, $from, $to, $this->interval));
        }

        return $this;
    }

    protected function getFrom()
    {
        if (empty($this->data)) {
            $dotenv = Dotenv::create(__DIR__ . '/../');
            $dotenv->load();
            return new DateTime(getenv('CANDLES_DATE_FROM'));
        } else {
            return end($this->data)->time;
        }
    }

    public function loadFromStorage()
    {
        $this->attachArray($this->storage->read());

        return $this;
    }

    public function attachArray(array $data)
    {
        parent::attachArray($data);

        $this->sortBy('time');

        return $this;
    }

    public function save()
    {
        $this->storage->write($this->toArray());

        return $this;
    }

}
