<?php

namespace TBot;

interface StorageInterface
{
    public function write(array $data);
    public function read() : array;
}
