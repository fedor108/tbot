<?php

namespace TBot;

require_once __DIR__ . '/../vendor/autoload.php';

$operations = new OperationCollection();
$operations->load()->save();

echo 'New operations count: ' . $operations->getApiCount() . PHP_EOL;
