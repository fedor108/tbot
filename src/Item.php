<?php

namespace TBot;

abstract class Item
{
    public function __construct($data = null)
    {
        if (!empty($data)) {
            $this->fromArray($data);
        }

        $this->init();
    }

    protected function init()
    {
        return $this;
    }

    public function fromArray(array $data)
    {
        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }

        return $this;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    public function toJson($data = [])
    {
        return empty($data) ? json_encode($this->toArray()) : json_encode($data);
    }

    public function getHash($data = [])
    {
        return md5($this->toJson($data));
    }

    public function getUniqueKey()
    {
        return $this->getHash();
    }
}
