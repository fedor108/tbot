<?php

namespace TBot;

use League\CLImate\CLImate;

require_once __DIR__ . '/../vendor/autoload.php';

$climate = new CLImate;

$climate->arguments->add([
    'type' => [
        'prefix'       => 't',
        'longPrefix'   => 'type',
        'description'  => 'Type of instrument',
        'defaultValue' => null,
    ],
    'currency' => [
        'prefix'      => 'c',
        'longPrefix'  => 'currency',
        'description' => 'Currency',
        'defaultValue' => null,
    ],
]);

$climate->arguments->parse();


$instruments = new InstrumentCollection();
$instruments->load();

$type = $climate->arguments->get('type');
if (!empty($type)) {
    $instruments->filterBy('type', $type);
}

$currency = $climate->arguments->get('currency');
if (!empty($currency)) {
    $instruments->filterBy('currency', $currency);
}

$toTable = $instruments->toArray();
$climate->out('Instruments');
$climate->table($toTable);
