<?php

namespace TBot;

use League\CLImate\CLImate;

require_once __DIR__ . '/../vendor/autoload.php';

$climate = new CLImate();

$climate->arguments->add([
    'buy' => [
        'prefix' => 'operations',
        'longPrefix' => 'operations',
        'description' => 'Show operations list',
        'noValue' => true,
    ],
    'sold' => [
        'prefix' => 'sold',
        'longPrefix' => 'sold',
        'description' => 'Show sold instruments',
        'noValue' => true,
    ],
    'hold' => [
        'prefix' => 'hold',
        'longPrefix' => 'hold',
        'description' => 'Show hold instruments',
        'noValue' => true,
    ],
]);

$climate->arguments->parse();

$operations = $climate->arguments->get('operations');
$sold = $climate->arguments->get('sold');
$hold = $climate->arguments->get('hold');

if (!($operations || $sold || $hold)) {
    $operations = $sold = $hold = true;
}

$accounts = new AccountCollection();
$accounts->load();

foreach ($accounts->toArray() as $key => $account) {

    array_walk($account['operations'], function (&$operation) {
        unset($operation['commission']);
        unset($operation['isMarginCall']);
        unset($operation['figi']);
    });

    $accountOperations = !empty($account['operations']) && $operations;
    $accountSold = !empty($account['output']) && $sold;
    $accountHold = !empty($account['hold']) && $hold;

    if ($accountOperations || $accountSold || $accountHold ) {
        $climate->out(PHP_EOL . PHP_EOL . '### ' . $account['title']);
    }

    if ($accountOperations) {
        $climate->out(PHP_EOL . 'Operations');
        $climate->table($account['operations']);
    }

    if ($accountSold) {
        array_walk($account['output'], function (&$output) {
            unset($output['outTimestamp']);
            unset($output['inputTimestamp']);
        });

        $climate->out(PHP_EOL . 'Sold');
        $climate->table($account['output']);
    }

    if ($accountHold) {
        $climate->out(PHP_EOL . 'Hold');
        $climate->table($account['hold']);
    }
}

$climate->out(PHP_EOL . '# Result');
$climate->table([$accounts->getProfitInfo()]);
