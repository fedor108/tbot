<?php

namespace TBot;

use GuzzleHttp\Client;
use Dotenv\Dotenv;
use DateTime;

class TClient
{
    const BASE_URI = 'https://api-invest.tinkoff.ru/openapi/';

    const SABDBOX_BASE_URI = 'https://api-invest.tinkoff.ru/openapi/sandbox/';

    private $httpClient;

    private $authToken;

    private $httpHeaders;

    public function __construct($params = [])
    {
        $this->httpClient = new Client([
            'base_uri' => empty($params['sandbox']) ? self::BASE_URI : self::SABDBOX_BASE_URI,
        ]);

        $dotenv = Dotenv::create(__DIR__ . '/../');
        $dotenv->load();
        $this->authToken = getenv('TINKOFF_TOKEN');

        $this->httpHeaders = [
            'Content-Type' => 'application/json',
            'Authorization' => "Bearer {$this->authToken}",
        ];
    }

    public function getOperations(DateTime $from, DateTime $to, $figi = null) : array
    {
        $params['query'] = [
            'from' => $from->format(DATE_ATOM),
            'to' => $to->format(DATE_ATOM),
            'figi' => $figi,
        ];

        $params['headers'] = $this->httpHeaders;

        $response = $this->httpClient->request('GET', 'operations', $params);

        return json_decode($response->getBody()->getContents(), true)['payload']['operations'];
    }

    public function getInstruments(string $type) : array
    {
        $params['headers'] = $this->httpHeaders;

        $response = $this->httpClient->request('GET', 'market/' . $type, $params);

        return json_decode($response->getBody()->getContents(), true)['payload']['instruments'];
    }

    public function getOrderbook(string $figi, int $depth)
    {
        $params['query'] = compact('figi', 'depth');

        $params['headers'] = $this->httpHeaders;

        $response = $this->httpClient->request('GET', 'market/orderbook', $params);

        return json_decode($response->getBody()->getContents(), true)['payload'];
    }

    public function getCandles(string $figi, DateTime $from, DateTime $to, string $interval)
    {
        $params['query'] = [
            'from' => $from->format(DATE_ATOM),
            'to' => $to->format(DATE_ATOM),
            'figi' => $figi,
            'interval' => $interval,
        ];

        $params['headers'] = $this->httpHeaders;

        $response = $this->httpClient->request('GET', 'market/candles', $params);

        try {
            $result = json_decode($response->getBody()->getContents(), true)['payload']['candles'];
        } catch (\Exception $exception) {
            throw new \Exception('Can\'t json_decode candles :' . $response->getBody()->getContents());
        }

        return $result;
    }

}
