<?php

namespace TBot;

use DateTime;

class Candle extends Item
{
    public $o;
    public $c;
    public $h;
    public $l;
    public $v;
    public $time;
    public $interval;
    public $figi;

    public function fromArray(array $data)
    {
        unset($data['date']);
        parent::fromArray($data);

        $this->setTime($data['time']);

        return $this;
    }

    public function toArray() : array
    {
        $result = parent::toArray();

        $result['time'] = $this->time->format('Y-m-d');

        return $result;
    }

    public function setTime($value)
    {
        if (is_numeric($value)) {
            $this->time = new DateTime('@' . $value);
        } elseif (is_string($value)) {
            $this->time = new DateTime($value);
        } elseif ($value instanceof DateTime) {
            $this->time = $value;
        } elseif (is_array($value)) {
            $this->time = new DateTime();
            $this->time->__set_state($value);
        } else {
            throw new Exception('Unknown type of value to set time');
        }

        return $this;
    }

    public function getUniqueKey()
    {
        return $this->time->getTimestamp();
    }
}
