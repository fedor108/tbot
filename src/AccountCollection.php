<?php

namespace TBot;

class AccountCollection extends Collection
{
    /**
     * @var InstrumentCollection $instruments
     */
    public $instruments;

    /**
     * @var OperationCollection $operations
     */
    public $operations;

    /**
     * @var OperationCollection $payInOperations
     */
    public $payInOperations;

    public $profit;
    public $percentProfit;
    public $percentByYearProfit;
    public $holdSum;
    public $payInSum;
    public $payInDays;
    public $payInYears;
    public $payInMonth;

    protected function init($params = [])
    {
        $this->itemClass = Account::class;

        return $this;
    }

    public function load()
    {
        $this->instruments = new InstrumentCollection();
        $this->instruments->load();

        $this->operations = new OperationCollection();
        $this->operations->load()
            ->filterBy('status', 'Done');

        $this->payInOperations = clone $this->operations;
        $this->payInOperations
            ->filterBy('operationType', [OperationType::PAY_IN, OperationType::PAY_OUT])
            ->filterBy('currency', 'RUB');

        $this->loadAccounts();

        $this->countProfit();

        return $this;
    }

    protected function loadAccounts()
    {
        foreach ($this->operations->data as $operation) {
            if ($operation->operationType == OperationType::PAY_IN) {
                $this->addOperationByName($operation->currency, $operation);
            } elseif ($operation->operationType == OperationType::PAY_OUT) {
                $this->addOperationByName($operation->currency, $operation);
            } elseif ($operation->operationType == OperationType::BUY) {
                $this->addOperationByName($operation->currency, $operation);
                $this->addOperationByName($operation->figi, $operation);
            } elseif ($operation->operationType == OperationType::BUY_CARD) {
                $this->addOperationByName($operation->currency, $operation);
                $this->addOperationByName($operation->figi, $operation);
            } elseif ($operation->operationType == OperationType::SELL) {
                $this->addOperationByName($operation->currency, $operation);
                $this->addOperationByName($operation->figi, $operation);
            } elseif ($operation->operationType == OperationType::SERVICE_COMMISSION) {
                $this->addOperationByName($operation->currency, $operation);
            } elseif ($operation->operationType == OperationType::TAX) {
                $this->addOperationByName($operation->currency, $operation);
            }
        }

        $this->countAccountProfits();

        return $this;
    }

    protected function addOperationByName(string $key, Operation $operation)
    {
        $account = [
            'instrument' => empty($operation->figi) ? null : $this->instruments->findByFigi($operation->figi),
            'currency' => $operation->currency,
        ];

        unset($operation->trades);

        $this->data[$key] = (empty($this->data[$key])) ? new Account($account) : $this->data[$key];
        $this->data[$key]->addOperation($operation);
    }

    protected function countProfit()
    {
        $this->countHoldSum();

        $this->payInSum = $this->payInOperations->getRealPaymentSum();

        $this->profit = $this->holdSum - $this->payInSum;

        $this->percentProfit = round(($this->holdSum - $this->payInSum) * 100 / $this->payInSum, 2);

        $this->payInDays = array_reduce($this->payInOperations->data, function ($carry, Operation $operation)  {
            $duration = (new \DateTime())->diff($operation->date)->days;
            $duration = empty($duration) ? 1 : $duration;

            $payInDays = $operation->realPayment * $duration;

            $carry += $payInDays;
            return $carry;

        }, 0);

        $this->payInYears = round($this->payInDays / 365, 2);

        $this->payInMonth = $this->payInYears * 12;

        $this->percentByYearProfit = round($this->profit * 100 * 365 / $this->payInDays, 2);

        return $this;
    }

    protected function countHoldSum()
    {
        $this->holdSum = array_reduce($this->data, function ($carry, Account $account) {
            $carry += $account->getHoldPaymentSum();
            return $carry;
        }, 0);

        $this->holdSum = round($this->holdSum, 2);

        return $this;
    }

    protected function countAccountProfits()
    {
        array_walk($this->data, function (Account $account) {
            $account->countProfit();
        });

        return $this;
    }

    public function getProfitInfo()
    {
        return [
            'payInSum' => $this->payInSum,
            'holdSum' => $this->holdSum,
            'profit' => $this->profit,
            'percentProfit' => $this->percentProfit,
            'percentByYearProfit' => $this->percentByYearProfit,
        ];
    }

}
