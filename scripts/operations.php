<?php

namespace TBot;

use League\CLImate\CLImate;

require_once __DIR__ . '/../vendor/autoload.php';

$climate = new CLImate;

$climate->arguments->add([
    'type' => [
        'prefix'       => 't',
        'longPrefix'   => 'type',
        'description'  => 'Type of operation',
        'defaultValue' => null,
    ],
    'currency' => [
        'prefix'      => 'c',
        'longPrefix'  => 'currency',
        'description' => 'Currency',
        'defaultValue' => 'RUB',
    ],

]);

$climate->arguments->parse();

$operations = new OperationCollection();
$operations->load();

$currency = $climate->arguments->get('currency');
if (!empty($currency)) {
    $operations->filterBy('currency', $currency);
}


/** @var array $toTable
 * table data of all operations
 */
$toTable = array_map(function ($item) {
    $res = $item;
    unset($res['trades']);

    if (!empty($item['commission'])) {
        $res['commission'] = $item['commission']['value'];
    } else {
        $res['commission'] = null;
    }

    $res['date'] = date('Y-m-d H:i', $item['date']);

    unset($res['commission']);

    return $res;
}, $operations->toArray());

$climate->out('Operations');
$climate->table($toTable);

/**
 * Trade operations only
 */
$trades = array_filter($toTable, function ($item) {
    return in_array($item['operationType'], [OperationType::SELL, OperationType::BUY, OperationType::BUY_CARD]) && ($item['status'] == 'Done');
});

array_walk($trades, function (&$item) {
    unset($item['isMarginCall']);
});

$climate->out('Trades');
$climate->table($trades);

$tradesSum = array_reduce($trades, function ($carry, $item) {
    $carry += $item['realPayment'];
    return $carry;
}, 0);


/**
 * Taxes only
 */
$taxes = array_filter($toTable, function ($item) {
    return in_array($item['operationType'], ['Tax', 'ServiceCommission', 'TaxDividend']) && ($item['status'] == 'Done');
});

array_walk($taxes, function (&$item) {
    unset($item['isMarginCall'], $item['quantity'], $item['price'], $item['commission'], $item['id']);
});

$taxesSum = array_reduce($taxes, function ($carry, $item) {
    $carry += $item['realPayment'];
    return $carry;
}, 0);

$climate->out('Taxes');
$climate->table($taxes);

/**
 * Payins only
 */
$payins = array_filter($toTable, function ($item) {
    return in_array($item['operationType'], ['PayIn']) && ($item['status'] == 'Done');
});

$payinSum = array_reduce($payins, function ($carry, $item) {
    $carry += $item['realPayment'];
    return $carry;
}, 0);

array_walk($payins, function (&$item) {
    unset($item['isMarginCall'], $item['quantity'], $item['price'], $item['commission'], $item['id'], $item['instrumentType'], $item['figi'], $item['commission']);
});

$climate->out('PayIns');
$climate->table($payins);

/**
 * Dividends only
 */
$dividends = array_filter($toTable, function ($item) {
    return in_array($item['operationType'], ['Dividend']) && ($item['status'] == 'Done');
});

$dividendSum = array_reduce($dividends, function ($carry, $item) {
    $carry += $item['realPayment'];
    return $carry;
}, 0);

array_walk($dividends, function (&$item) {
    unset($item['isMarginCall'], $item['quantity'], $item['price'], $item['commission'], $item['id'], $item['instrumentType'], $item['figi'], $item['commission']);
});

$climate->out('Dividends');
$climate->table($dividends);

/**
 * Balance
 */
$sum = round($payinSum + $taxesSum + $tradesSum + $dividendSum, 2);
$climate->out(print_r(compact('payinSum', 'taxesSum', 'tradesSum', 'sum'), true));
