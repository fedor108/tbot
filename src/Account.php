<?php

namespace TBot;

use Exception;

class Account extends Item
{
    const BROKER_COMMISSION = 0.003;

    /**
     * @var string $type
     */
    public $type;

    /**
     * @var string $title
     */
    public $title;

    /**
     * @var Instrument $instrument
     */
    public $instrument;

    /**
     * @var string $currency
     */
    public $currency;

    /**
     * @var OperationCollection
     */
    public $operations;

    /**
     * @var array $output
     */
    public $output;

    /**
     * @var array $input
     */
    public $input;

    /**
     * @var array $hold
     */
    public $hold;

    protected function init()
    {
        $this->type = empty($this->instrument) ? 'money' : 'paper';

        $this->title = empty($this->instrument) ? $this->currency : $this->instrument->title;

        $this->operations = new OperationCollection();
    }

    public function addOperation(Operation $operation)
    {
        $this->operations->attachArray([$operation]);
        $this->operations->sortBy('date');
    }

    public function toArray()
    {
        $arr = parent::toArray();

        $arr['operations'] = $this->operations->toArray();

        return $arr;
    }

    public function countProfit()
    {
        $this->input = [];
        $this->output = [];

        foreach ($this->operations->data as $operation) {
            /**
             * @var Operation $operation
             */
            if ($this->type == 'paper') {
                if ($operation->payment < 0) {
                    $this->addPaperInput($operation);
                } else {
                    $this->addPaperOut($operation);
                }
            } elseif ($this->type == 'money') {
                if ($operation->payment > 0) {
                    $this->addMoneyInput($operation);
                } else {
                    $this->addMoneyOut($operation);
                }
            } else {
                throw new Exception('Unknown type of account: ' . $this->type);
            }
        }

        if ($this->type == 'paper') {
            $this->setPaperHold();
        } else {
            $this->setMoneyHold();
        }

        return $this;
    }

    protected function addPaperInput(Operation $operation)
    {
        $this->input[] = [
            'quantity' => $operation->quantity,
            'operationKey' => $this->operations->search($operation),
        ];

        return $this;
    }

    public function addPaperOut(Operation $outOperation)
    {
        /**
         * @var int $needQuantity
         */
        $needQuantity = $outOperation->quantity;

        foreach ($this->input as $inputKey => $input) {
            if ($input['quantity'] == 0) {
                continue;
            }

            /**
             * @var int $quantity
             */
            $quantity = min($needQuantity, $input['quantity']);

            /**
             * @var Operation $inputOperation
             */
            $inputOperation = $this->operations->data[$input['operationKey']];
            $inputPrice = $inputOperation->price;
            $inputPayment = abs($inputOperation->realPayment) * $quantity / $inputOperation->quantity;
            $inputDate = $inputOperation->date->format('Y-m-d');
            $inputTimestamp = $inputOperation->date->getTimestamp();

            $outPrice = $outOperation->price;
            $outPayment = round(($outOperation->realPayment) * $quantity / $outOperation->quantity, 2);
            $outDate = $outOperation->date->format('Y-m-d');
            $outTimestamp = $outOperation->date->getTimestamp();

            $duration = self::getNotNullDuration($outOperation->date->diff($inputOperation->date)->days);
            $profit = round($outPayment - $inputPayment, 2);
            $percentProfit = $this->getPercentProfit($profit, $inputPayment);
            $percentByYearProfit = $this->getPercentByYearProfit($profit, $inputPayment, $duration);

            $this->output[] = compact(
                'outDate',
                'duration',
                'quantity',
                'inputDate',
                'profit',
                'percentProfit',
                'percentByYearProfit',
                'outPrice',
                'outPayment',
                'inputPrice',
                'inputPayment',
                'outTimestamp',
                'inputTimestamp'
            );

            $this->input[$inputKey]['quantity'] -= $quantity;

            $needQuantity -= $quantity;
            if ($needQuantity == 0) {
                break;
            }

            if ($needQuantity < 0) {
                throw new Exception('sell quantity > buy quantity');
            }
        }

        return $this;
    }

    public function setPaperHold()
    {
        foreach ($this->input as $inputKey => $input) {
            $quantity = $input['quantity'];
            if ($quantity == 0) {
                continue;
            }

            /**
             * @var Operation $inputOperation
             */
            $inputOperation = $this->operations->data[$input['operationKey']];
            $inputPrice = $inputOperation->price;
            $inputPayment = abs($inputOperation->realPayment) * $quantity / $inputOperation->quantity;
            $inputDate = $inputOperation->date->format('Y-m-d');

            $outPrice = $this->instrument->getCurrentPrice();
            $outPayment = round($outPrice * (1 - self::BROKER_COMMISSION) * $quantity, 2);
            $outDate = (new \DateTime('now'))->format('Y-m-d');

            $duration = self::getNotNullDuration((new \DateTime('now'))->diff($inputOperation->date)->days);
            $profit = round($outPayment - $inputPayment, 2);
            $percentProfit = $this->getPercentProfit($profit, $inputPayment);
            $percentByYearProfit = $this->getPercentByYearProfit($profit, $inputPayment, $duration);

            $this->hold[] = compact(
                'outDate',
                'duration',
                'quantity',
                'inputDate',
                'profit',
                'percentProfit',
                'percentByYearProfit',
                'outPrice',
                'outPayment',
                'inputPrice',
                'inputPayment',
                'inputTimestamp'
            );
        }

        return $this;
    }

    public function addMoneyInput(Operation $operation)
    {
        $this->input[] = [
            'payment' => abs($operation->realPayment),
            'operationKey' => $this->operations->search($operation),
            'date' => $operation->date->format('Y-m-d'),
            'timestamp' => $operation->date->getTimestamp(),
        ];

        return $this;
    }

    public function addMoneyOut(Operation $outOperation)
    {
        $needPayment = abs($outOperation->realPayment);

        foreach ($this->input as $inputKey => $input) {
            if ($input['payment'] <= 0) {
                continue;
            }

            /**
             * @var Operation $inputOperation
             */
            $inputOperation = $this->operations->data[$input['operationKey']];
            $inputDate = $inputOperation->date->format('Y-m-d');
            $inputTimestamp = $inputOperation->date->getTimestamp();

            $payment = min($needPayment, abs($input['payment']));
            $duration = self::getNotNullDuration($outOperation->date->diff($inputOperation->date)->days);

            $this->input[$inputKey]['payment'] -= $payment;

            $outTitle = $outOperation->operationType
                . ' ' . $outOperation->payment
                . ' ' . $outOperation->figi;

            $outDate = $outOperation->date->format('Y-m-d');
            $outTimestamp = $outOperation->date->getTimestamp();

            $this->output[] = compact(
                'outDate',
                'duration',
                'inputDate',
                'payment',
                'outTimestamp',
                'inputTimestamp',
                'outTitle'
            );

            $needPayment -= $payment;

            if ($needPayment <= 0) {
                break;
            }
        }

        return $this;
    }

    public function setMoneyHold()
    {
        foreach ($this->input as $inputKey => $input) {
            if ($input['payment'] <= 0) {
                continue;
            }

            /**
             * @var Operation $inputOperation
             */
            $inputOperation = $this->operations->data[$input['operationKey']];
            $inputDate = $inputOperation->date->format('Y-m-d');
            $inputTimestamp = $inputOperation->date->getTimestamp();

            $outPayment = round($input['payment'], 2);
            $outDate = (new \DateTime('now'))->format('Y-m-d');

            $duration = self::getNotNullDuration((new \DateTime('now'))->diff($inputOperation->date)->days);

            $this->hold[] = compact(
                'outDate',
                'duration',
                'inputDate',
                'outPayment',
                'inputTimestamp'
            );
        }
    }

    protected function getPercentProfit($profit, $payment)
    {
        return round($profit * 100 / abs($payment), 2);
    }

    protected function getPercentByYearProfit($profit, $payment, $duration)
    {
        return round($profit * 100 * 365 / abs($payment * $duration), 2);
    }

    public function getHoldPaymentSum()
    {
        return empty($this->hold) ? 0 : array_sum(array_column($this->hold, 'outPayment'));
    }

    public static function getNotNullDuration($duration)
    {
        return ($duration == 0) ? 1 : $duration;
    }
}
