<?php

namespace TBot;

use DateTime;
use League\CLImate\CLImate;

require_once __DIR__. '/../vendor/autoload.php';

$climate = new CLImate();

$climate->arguments->add([
    'figi' => [
        'prefix' => 'figi',
        'longPrefix' => 'figi',
        'description' => 'Figi of instrument',
    ],
    'from' => [
        'prefix' => 'from',
        'longPrefix' => 'from',
        'description' => 'Date to load candles from',
    ],
    'to' => [
        'prefix' => 'to',
        'longPrefix' => 'to',
        'description' => 'Date to load candles to',
    ],
    'interval' => [
        'prefix' => 'interval',
        'longPrefix' => 'interval',
        'description' => 'Available values : 1min, 2min, 3min, 5min, 10min, 15min, 30min, hour, day, week, month',
        'defaultValue' => 'day',
    ],
]);

require_once __DIR__. '/../vendor/autoload.php';

$climate->arguments->parse();

$params = [];
$params['figi'] = $climate->arguments->get('figi');
$params['interval'] = $climate->arguments->get('interval');

if (!empty($climate->arguments->get('from'))) {
    $params['from'] = new DateTime($climate->arguments->get('from'));
}

if (!empty($climate->arguments->get('to'))) {
    $params['to'] = new DateTime($climate->arguments->get('to'));
}

if (empty($params['figi'])) {
    $instruments = new InstrumentCollection();
    $instruments->load()
        ->filterBy('currency', 'RUB')
        ->filterBy('type', ['Stock', 'Eft']);
    foreach ($instruments->data as $instrument) {
        /**
         * @var Instrument $instrument
         */
        $params['figi'] = $instrument->figi;

        $climate->out("{$instrument->name} - {$instrument->ticker} - {$instrument->figi}");

        $candles = new CandleCollection($params);
        $candles->load($params)->save();

        $climate->out($candles->getCount() . PHP_EOL);
    }
} else {
    $candles = new CandleCollection($params);
    $candles->load($params)->save();

    $climate->out($params['figi']);
    $climate->out($candles->getCount());
}

