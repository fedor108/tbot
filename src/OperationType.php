<?php

namespace TBot;

class OperationType
{
    const PAY_IN = 'PayIn';
    const PAY_OUT = 'PayOut';
    const BUY = 'Buy';
    const BUY_CARD = 'BuyCard';
    const SELL = 'Sell';
    const SERVICE_COMMISSION = 'ServiceCommission';
    const TAX = 'Tax';
}
