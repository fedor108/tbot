#!/bin/bash

set -e
set -u

function create_user_and_database() {
	local database=$(echo $1 | tr ',' ' ' | awk  '{print $1}')
	local owner=$(echo $1 | tr ',' ' ' | awk  '{print $2}')
  local password=$(echo $1 | tr ',' ' ' | awk  '{print $3}')
	echo "  Creating user '$owner' with pass '$password' and database '$database'"
	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER $owner with password '$password';
	    CREATE DATABASE $database owner $owner;
      GRANT ALL PRIVILEGES ON DATABASE $database to $owner;
      ALTER USER $owner WITH SUPERUSER;
EOSQL
}

if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
	echo "Database creation requested: $POSTGRES_MULTIPLE_DATABASES"
	for db in $(echo $POSTGRES_MULTIPLE_DATABASES | tr ':' ' '); do
		create_user_and_database $db
	done
	echo "Database created"
fi
