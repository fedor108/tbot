<?php

namespace TBot;

use Exception;

class InstrumentCollection extends Collection
{
    protected $storage;

    protected function init($params = [])
    {
        $this->itemClass = Instrument::class;

        $this->storage = new FileStorage('instruments');

        return $this;
    }

    public function loadFromStorage()
    {
        $this->attachArray($this->storage->read());

        return $this;
    }

    public function attachInstruments(array $data, string $type)
    {
        array_walk($data, function (&$item) use ($type) {
            $item['type'] = $type;
        });

        parent::attachArray($data);

        return $this;
    }

    public function load($params = [])
    {
        $this->loadFromStorage();

        if (empty($this->data)) {
            $this->attachInstruments($this->client->getInstruments('stocks'), 'Stock');
            $this->attachInstruments($this->client->getInstruments('bonds'), 'Bond');
            $this->attachInstruments($this->client->getInstruments('etfs'), 'Etf');
            $this->attachInstruments($this->client->getInstruments('currencies'), 'Currency');
        }

        return $this;
    }

    public function save()
    {
        return $this->storage->write($this->toArray());
    }

    public function findByFigi(string $figi) : Instrument
    {
        foreach ($this->data as $instrument) {
            if ($instrument->figi == $figi) {
                return $instrument;
            }
        }

        throw new Exception('Not found instrument by figi: ' . $figi);
    }

}
