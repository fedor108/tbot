<?php

namespace TBot;

use Dotenv\Dotenv;

class FileStorage implements StorageInterface
{
    protected $type;
    protected $file;

    public function __construct(string $type)
    {
        $this->type = $type;

        $dotenv = Dotenv::create(__DIR__ . '/../');
        $dotenv->load();

        $this->file = getenv('STORAGE_PATH') . $this->type . '.json';
    }

    public function write(array $data)
    {
        return file_put_contents($this->file, json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    }

    public function read() : array
    {
        $fileName = $this->file;
        if (!file_exists($fileName)) {
            return [];
        }

        $content = file_get_contents($this->file);
        if (empty(trim($content))) {
            return [];
        }

        return json_decode($content, true);
    }

}
