<?php

namespace TBot;

abstract class Collection
{
    protected $client;

    public $data = [];

    public $itemClass;

    public function __construct($params = [])
    {
        $this->client = new TClient;

        $this->init($params);
    }

    protected function init($params = [])
    {
        return $this;
    }

    public function attachArray(array $data)
    {
        if (empty($data)) {
            return $this;
        }

        foreach ($data as $item) {
            if (empty($item)) {
                continue;
            }

            $obj = ($item instanceof $this->itemClass) ? $item : new $this->itemClass($item);

            $this->data[$obj->getUniqueKey()] = $obj;
        }

        return $this;
    }

    public function addItemByKey($key, $item)
    {
        $obj = ($item instanceof $this->itemClass) ? $item : new $this->itemClass($item);

        $this->data[$key] = $obj;

        return $this;
    }

    public function toArray() : array
    {
        $result = [];

        foreach ($this->data as $key => $item) {
            /**
             * @var Item $item
             */
            $arr = $item->toArray();
            array_walk_recursive($arr, function (&$value) {
                if (is_object($value) && method_exists($value, 'toArray')) {
                    $value = $value->toArray();
                }
            });
            $result[$key] = $arr;
        }

        return $result;
    }

    public function filterBy($key, $value)
    {
        $this->data = array_filter($this->data, function ($item) use ($key, $value) {
            return (is_array($value)) ? in_array($item->{$key}, $value) : $value == $item->{$key};
        });

        return $this;
    }

    public function ksort()
    {
        ksort($this->data);
        return $this;
    }

    public function sortBy($field)
    {
        uasort($this->data, function ($a, $b) use ($field) {
            return $a->{$field} <=> $b->{$field};
        });
        return $this;
    }

    public function clearEmptyValues()
    {
        array_walk($this->data, function ($item) {
            $item->clearEmptyValues();
        });

        return $this;
    }

    public function search($item)
    {
        return array_search($item, $this->data);
    }

    public function getCount()
    {
        return count($this->data);
    }

}

